<?php

namespace Drupal\entity_word\Plugin\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Class EntityDownloadDocumentTab to get route parameters.
 */
class EntityDownloadDocumentTab extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $node = $route_match->getParameter('node');
    return [
      'node_id' => $node->id(),
    ];
  }

}
